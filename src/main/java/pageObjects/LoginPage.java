package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import factories.HLUtilities;
import junit.framework.Assert;

public class LoginPage extends HLUtilities{
	private WebDriver driver = null;
	
	@FindBy(how = How.NAME, using = "userName")
	WebElement Username;
	@FindBy(how = How.NAME, using = "password")
	WebElement Password;
	@FindBy(how = How.TAG_NAME, using = "button")
	WebElement LoginButton;
	@FindBy(how = How.CLASS_NAME, using = "mb-20")
	WebElement LoginText;
	
	
	
	public LoginPage(WebDriver ScriptDriver){
		driver = ScriptDriver;
		PageFactory.initElements(driver, this);
	}

	public LandingPage login(String email, String password) throws InterruptedException {
		LandingPage landingPage = null;
		
		Assert.assertTrue(verifyText(LoginText,"Log In"));
		
		if(Username.isDisplayed() && Username.isEnabled()){
			Username.sendKeys(email);
		}
		if(Password.isDisplayed() && Password.isEnabled()){
			Password.sendKeys(password);
		}
		if(LoginButton.isDisplayed() && LoginButton.isEnabled()){
			//WebDriverWait wait = new WebDriverWait(driver, 30);
			//wait.until(ExpectedConditions.)
			//Thread.sleep(3000);
			LoginButton.click();
			if(!LoginButton.isDisplayed()){
				landingPage = new LandingPage(driver);
			}
		}
		return landingPage;	
	}
	
	
	
}
