package pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import factories.HLUtilities;
import junit.framework.Assert;

public class LandingPage extends HLUtilities{
	private WebDriver driver = null;

	@FindBy(how = How.CLASS_NAME, using = "LoginText")
	public WebElement DashboardText;
	@FindBy(how = How.CLASS_NAME, using = "dropdown-label")
	public List<WebElement> LoggedinUsername;

	public LandingPage(WebDriver ScriptDriver){
		driver = ScriptDriver;
		PageFactory.initElements(driver, this);
		Assert.assertTrue(verifyText(DashboardText,"Dashboard"));
		Assert.assertTrue(verifyText(LoggedinUsername.get(2),"Admin Lexicon"));
	}	

}
