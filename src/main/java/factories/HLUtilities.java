package factories;

import org.openqa.selenium.WebElement;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class HLUtilities {
	
	public boolean verifyText(WebElement element, String text){
		boolean result = false;
		if(element.getText().toString().equals(text)){
			result = true;
		}
		return result;
	}
}
